import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { carsReducer } from '../reducers/carsReducer';
import { editCarIdReducer } from '../reducers/editCarIdReducer';
import { isLoadingReducer } from '../reducers/isLoadingReducer';

export const carToolStore = createStore(
  // (state, action) => newState
  combineReducers({
    cars: carsReducer,
    editCarId: editCarIdReducer,
    isLoading: isLoadingReducer,
  }),
  composeWithDevTools(applyMiddleware(thunk)),
);
