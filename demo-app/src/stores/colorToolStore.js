import { createStore } from 'redux';
import { colorToolReducer } from '../reducers/colorToolReducer';

export const colorToolStore = createStore(colorToolReducer);
