export const REFRESH_CARS_REQUEST = 'REFRESH_CARS_REQUEST';
export const REFRESH_CARS_DONE = 'REFRESH_CARS_DONE';
export const APPEND_CAR_REQUEST = 'APPEND_CAR_REQUEST';
export const APPEND_CAR_DONE = 'APPEND_CAR_DONE';
export const REPLACE_CAR_REQUEST = 'REPLACE_CAR_REQUEST';
export const REPLACE_CAR_DONE = 'REPLACE_CAR_DONE';
export const DELETE_CAR_REQUEST = 'DELETE_CAR_REQUEST';
export const DELETE_CAR_DONE = 'DELETE_CAR_DONE';
export const EDIT_CAR = 'EDIT_CAR';
export const CANCEL_CAR = 'CANCEL_CAR';

export const createRefreshCarsRequestAction = () => ({ type: REFRESH_CARS_REQUEST });
export const createRefreshCarsDoneAction = value => ({ type: REFRESH_CARS_DONE, value });
export const createAppendCarRequestAction = value => ({ type: APPEND_CAR_REQUEST, value });
export const createAppendCarDoneAction = value => ({ type: APPEND_CAR_DONE, value });
export const createReplaceCarRequestAction = value => ({ type: REPLACE_CAR_REQUEST, value });
export const createReplaceCarDoneAction = value => ({ type: REPLACE_CAR_DONE, value });
export const createDeleteCarRequestAction = value => ({ type: DELETE_CAR_REQUEST, value });
export const createDeleteCarDoneAction = value => ({ type: DELETE_CAR_DONE, value });
export const createEditCarAction = value => ({ type: EDIT_CAR, value });
export const createCancelCarAction = () => ({ type: CANCEL_CAR });

export const refreshCars = () => {

  // the middleware will invoke this function
  // passing a reference to the dispatch function
  return dispatch => {

    dispatch(createRefreshCarsRequestAction());
    return fetch('http://localhost:3050/cars')
      .then(res => res.json())
      .then(cars => dispatch(createRefreshCarsDoneAction(cars)));

  };

};

export const appendCar = (car) => {

  return dispatch => {

    dispatch(createAppendCarRequestAction(car));
    return fetch('http://localhost:3050/cars', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(car)
    })
      .then(res => res.json())
      .then(car => dispatch(createAppendCarDoneAction(car)))
      // value => calcStore.dispatch(createAddAction(value));
      // (...params) => dispatch(refreshCars(...params))
      .then(() => dispatch(refreshCars()));

  };

}

export const replaceCar = (car) => {

  return dispatch => {

    dispatch(createReplaceCarRequestAction(car));
    return fetch('http://localhost:3050/cars/' + encodeURIComponent(car.id), {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(car)
    })
      .then(res => res.json())
      .then(car => dispatch(createReplaceCarDoneAction(car)))
      // value => calcStore.dispatch(createAddAction(value));
      // (...params) => dispatch(refreshCars(...params))
      .then(() => dispatch(refreshCars()));

  };

}

export const deleteCar = (carId) => {

  return dispatch => {

    dispatch(createDeleteCarRequestAction(carId));
    return fetch('http://localhost:3050/cars/' + encodeURIComponent(carId), {
      method: 'DELETE',
    })
      .then(res => res.json())
      .then(car => dispatch(createDeleteCarDoneAction(carId)))
      // value => calcStore.dispatch(createAddAction(value));
      // (...params) => dispatch(refreshCars(...params))
      .then(() => dispatch(refreshCars()));

  };

}
