export const ADD_COLOR = 'ADD_COLOR';
export const DELETE_COLOR = 'DELETE_COLOR';

export const createAddColorAction = value =>
  ({ type: ADD_COLOR, value });
export const createDeleteColorAction = value =>
  ({ type: DELETE_COLOR, value });
