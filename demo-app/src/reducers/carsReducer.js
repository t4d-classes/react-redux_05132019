import {
  REFRESH_CARS_DONE,
} from '../actions/car-tool.actions';

export const carsReducer = (state = [], action) => {

  if (action.type === REFRESH_CARS_DONE) {
    return action.value;
  }

  return state;

};