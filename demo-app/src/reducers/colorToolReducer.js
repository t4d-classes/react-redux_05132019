import { ADD_COLOR, DELETE_COLOR } from '../actions/color-tool.actions';

const colorList = [
  { id: 1, name: 'pink', hexcode: '#dddddd' },
  { id: 2, name: 'black', hexcode: '#000000' },
];

export const colorToolReducer = (state = colorList, action) => {

  switch (action.type) {
    case ADD_COLOR:
      return state.concat({
        ...action.value,
        id: Math.max(...state.map(c => c.id), 0) + 1,
      });
    case DELETE_COLOR:
      return state.filter(c => c.id !== action.value);
    default:
      return state;
  }

};