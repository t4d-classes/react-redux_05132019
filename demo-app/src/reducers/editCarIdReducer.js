import {EDIT_CAR, REFRESH_CARS_DONE,
} from '../actions/car-tool.actions';

export const editCarIdReducer = (state = -1, action) => {

  if (action.type === REFRESH_CARS_DONE) {
    return -1;
  }

  if (action.type === EDIT_CAR) {
    return action.value;
  }

  return state;
};