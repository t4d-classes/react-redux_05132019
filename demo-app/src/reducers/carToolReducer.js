import {
  APPEND_CAR_REQUEST, REPLACE_CAR_REQUEST, DELETE_CAR_REQUEST,
  EDIT_CAR, CANCEL_CAR, REFRESH_CARS_REQUEST, REFRESH_CARS_DONE,
} from '../actions/car-tool.actions';

export const carToolReducer = (state = { cars: [], editCarId: -1, isLoading: false }, action) => {

  switch (action.type) {
    case REFRESH_CARS_REQUEST:
    case APPEND_CAR_REQUEST:
    case REPLACE_CAR_REQUEST:
    case DELETE_CAR_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case REFRESH_CARS_DONE:
      return {
        ...state,
        isLoading: false,
        cars: action.value,
        editCarId: -1,
      };
    case EDIT_CAR:
      return {
        ...state,
        editCarId: action.value,
      };
    case CANCEL_CAR:
      return {
        ...state,
        editCarId: -1,
      };
    default:
      return state;
  }

};