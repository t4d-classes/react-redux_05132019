import React, { useEffect } from 'react';

import { ToolHeader } from './ToolHeader';
import { CarTable } from './CarTable';
import { CarForm } from './CarForm';

export const CarTool = (props) => {

  useEffect(() => {
    props.onRefreshCars();
  }, []);

  return (
    <>
      <ToolHeader headerText="Car Tool" />
      <CarTable cars={props.cars} editCarId={props.editCarId}
        onEditCar={props.onEditCar} onDeleteCar={props.onDeleteCar}
        onSaveCar={props.onReplaceCar} onCancelCar={props.onCancelCar} />
      <CarForm buttonText="Add Car" onSubmitCar={props.onAppendCar} />
    </>
  );

};