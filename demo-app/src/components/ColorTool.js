import React, { useState } from 'react';

import { ToolHeader } from './ToolHeader';

export const ColorTool = (props) => {

  const [ colorForm, setColorForm ] = useState({
    color: '',
    hexcode: '',
  });

  const change = (e) => {
    setColorForm({
      ...colorForm,
      [ e.target.name ]: e.target.value,
    });
  };

  return (
    <>
      <ToolHeader headerText="Color Tool" />
      <ul>
        {props.colors.map((color, index) =>
          <li key={index}>
            {color.name}
            <button type="button" onClick={() => props.onDeleteColor(color.id)}>X</button>
          </li>)}
      </ul>
      <form>

        <div>
          <label htmlFor="color-input">Color:</label>
          <input type="text" id="color-input" value={colorForm.name}
            onChange={change} name="name" />
        </div>

        <div>
          <label htmlFor="hexcode-input">Hex Code:</label>
          <input type="text" id="hexcode-input" value={colorForm.hexcode}
            onChange={change} name="hexcode" />
        </div>

        <button type="button" onClick={() => props.onAddColor({ ...colorForm })}>Add Color</button>

      </form>
    </>
  );

};