import React from 'react';

export const ToolHeader = ({ headerText }) =>
  <header>
    <h1>{headerText}</h1>
  </header>;
