import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { colorToolStore } from './stores/colorToolStore';
import { ColorToolContainer } from './containers/ColorToolContainer';

ReactDOM.render(
  <Provider store={colorToolStore}>
    <ColorToolContainer />
  </Provider>,
  document.querySelector('#root'),
);


