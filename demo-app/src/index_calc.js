import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { createStore, bindActionCreators } from 'redux';
import { Provider, connect } from 'react-redux';

const ADD = '[Calc] Add';
const SUBTRACT = '[Calc] Subtract';
const MULTIPLY = '[Calc] Multiply';
const DIVIDE = '[Calc] Divide';

const createAddAction = value => ({ type: ADD, value });
const createSubtractAction = value => ({ type: SUBTRACT, value });
const createMultiplyAction = value => ({ type: MULTIPLY, value });
const createDivideAction = value => ({ type: DIVIDE, value });

const calcReducer = (state = { result: 0, history: [] }, action) => {
  console.log('state:', state, 'action:', action);
  switch(action.type) {
    case ADD:
      return {
        result: state.result + action.value,
        history: state.history.concat({ opName: 'Add', opValue: action.value }),
      };
    case SUBTRACT:
      return {
        result: state.result - action.value,
        history: state.history.concat({ opName: 'Subtract', opValue: action.value }),
      };
    case MULTIPLY:
      return {
        result: state.result * action.value,
        history: state.history.concat({ opName: 'Multiply', opValue: action.value }),
      };
    case DIVIDE:
      return {
        result: state.result / action.value,
        history: state.history.concat({ opName: 'Divide', opValue: action.value }),
      };
    default:
      return state;
  }
};

// const state = actions.reduce( calcReducer, 0 /* initial value of the state */ );
// console.log(state);

// const createStore = reducerFn => {

//   let currentState = undefined;
//   const subscribers = [];

//   return {
//     getState: () => currentState,
//     dispatch: (action) => {
//       currentState = reducerFn(currentState, action);
//       subscribers.forEach(callbackFn => callbackFn());
//     },
//     subscribe: (callbackFn) => {
//       subscribers.push(callbackFn);
//     },
//   };

// };

const calcStore = createStore(calcReducer);

// calcStore.subscribe(() => {
//   console.log('state: ', calcStore.getState());

//   ReactDOM.render(
//     <CalcTool
//       result={calcStore.getState().result} history={calcStore.getState().history}
//       onAdd={add} onSubtract={subtract} onMultiply={multiply} onDivide={divide} />,
//     document.querySelector('#root'),
//   );
// });

// runs each time state changes
const mapStateToProps = (state) => ({
  result: state.result,
  history: state.history,
});

// runs once when bringing the component to life
const mapDispatchToProps = (dispatch) => bindActionCreators({
  onAdd: createAddAction,
  onSubtract: createSubtractAction,
  onMultiply: createMultiplyAction,
  onDivide: createDivideAction,
}, dispatch);

// const { Provider, Consumer } = React.createContext(null);

// const connect = (mapStateToPropsFn, mapDispatchToPropsFn) => {

//   return PresentationalComponent => {

//     class ContainerComponent extends React.Component {

//       constructor(props) {
//         super(props);

//         this.dispatchFns = mapDispatchToPropsFn(this.props.store.dispatch);
//       }

//       componentDidMount() {
//         this.unsubscribeFromStore = this.props.store.subscribe(() => {
//           this.forceUpdate();
//         });
//       }

//       componentWillUnmount() {
//         this.unsubscribeFromStore();
//       }

//       render() {

//         const stateProps = mapStateToPropsFn(this.props.store.getState());

//         return <PresentationalComponent {...this.props}
//           {...this.dispatchFns} {...stateProps} />;
//       }

//     }

//     return () => <Consumer>{value => <ContainerComponent store={value} />}</Consumer>;

//   };

// };

// const add = value => calcStore.dispatch(createAddAction(value));

// const bindActionCreators = (actionCreatorMap, dispatch) =>
//   Object.keys(actionCreatorMap).reduce( (actionMap, actionKey) => {
//     actionMap[actionKey] = (...params) =>
//       dispatch(actionCreatorMap[actionKey](...params));
//     return actionMap
//   }, {});

// const { add, subtract, multiply, divide } = bindActionCreators({
//   add: createAddAction,
//   subtract: createSubtractAction,
//   multiply: createMultiplyAction,
//   divide: createDivideAction,
// }, calcStore.dispatch);

const CalcTool = ({
  result, history,
  onAdd: add, onSubtract: subtract,
  onMultiply: multiply, onDivide: divide,
 }) => {

  const [ inputValue, setInputValue ] = useState(0);

  return <form>
    <div>
      Result: {result}
    </div>
    <div>
      <label htmlFor="input-value">Input:</label>
      <input type="number" id="input-value" value={inputValue}
        onChange={({ target: { value }}) => setInputValue(Number(value))} />
    </div>
    <div>
      <button type="button" onClick={() => add(inputValue)}>+</button>
      <button type="button" onClick={() => subtract(inputValue)}>-</button>
      <button type="button" onClick={() => multiply(inputValue)}>*</button>
      <button type="button" onClick={() => divide(inputValue)}>/</button>
    </div>
    <h3>History</h3>
    <ul>
      {history.map(h => <li>{h.opName} {h.opValue}</li>)}
    </ul>

  </form>;

};

const createCalcToolContainer = connect(mapStateToProps, mapDispatchToProps);

const CalcToolContainer = createCalcToolContainer(CalcTool);

ReactDOM.render(
  <Provider store={calcStore}>
    <CalcToolContainer />
  </Provider>,
  document.querySelector('#root'),
);


