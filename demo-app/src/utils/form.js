import { useState } from 'react';

export const useForm = (initialForm) => {
  
  const [ form, setForm ] = useState(initialForm);

  return [
    form,
    ({ target: { type, name, value } }) => {
      setForm({
        ...form,
        [ name ]: type === 'number'
          ? Number(value)
          : value,
      });
    },
  ];
};
