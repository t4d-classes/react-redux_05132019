import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actions from '../actions/car-tool.actions';

import { CarTool } from '../components/CarTool';

export const CarToolContainer = connect(
  // mapStateToProps function
  ({ cars, editCarId }) => ({ cars, editCarId }),
  // mapDispatchToProps function
  (dispatch) => bindActionCreators({
    // onRefreshCars: (...params) => dispatch(refreshCars(...params))
    onRefreshCars: actions.refreshCars,
    onAppendCar: actions.appendCar,
    onReplaceCar: actions.replaceCar,
    onDeleteCar: actions.deleteCar,
    onEditCar: actions.createEditCarAction,
    onCancelCar: actions.createCancelCarAction,
  }, dispatch),
)(CarTool);