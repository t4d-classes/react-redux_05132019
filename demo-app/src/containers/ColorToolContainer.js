import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  createAddColorAction,
  createDeleteColorAction,
} from '../actions/color-tool.actions';

import { ColorTool } from '../components/ColorTool';

export const ColorToolContainer = connect(
  // mapStateToProps function
  (cars) => ({ colors: colors }),
  // mapDispatchToProps function
  (dispatch) => bindActionCreators({
    onAddColor: createAddColorAction,
    onDeleteColor: createDeleteColorAction,
  }, dispatch),
)(ColorTool);