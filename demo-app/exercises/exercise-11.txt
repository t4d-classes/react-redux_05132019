Exercise 11

1. Create a CalcTool component as described on the board.

The only state which can be managed within the component is the state to capture the input value.

2. Connect the Redux store and related logic to the CalcTool component.

3. Ensure it works.